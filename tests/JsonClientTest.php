<?php
namespace Yergo\Tests;

use Yergo\JsonClient;
use Yergo\Tests\JsonClient\TestCase;

class JsonClientTest extends TestCase
{

    /**
     * @var JsonClient
     */
    protected $client;

    public function testCanDoGetRequest()
    {
        $answer = $this->client->get('posts/1');

        $this->assertFalse($answer->isAvailable(), 'Directly after request its abnormal to have answer');
        $array = $answer->get();
        $this->assertNotEmpty($array);

        $answer = $this->client->get('posts/2');

        $this->assertFalse($answer->isAvailable(), 'Directly after request its abnormal to have answer');
        $array = $answer->get();
        $this->assertNotEmpty($array);
    }

    public function testCanDoPostRequest()
    {
        $item = $this->client->post([
            "title" => 'foo',
            "body" => 'bar',
            "userId" => 1
        ], 'posts')->get();

        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('title', $item);
        $this->assertArrayHasKey('body', $item);
        $this->assertArrayHasKey('userId', $item);

        $this->assertEquals('bar', $item['body']);
    }

    public function testCanDoPutRequest()
    {
        $item = $this->client->put([
            "id" => 1,
            "title" => 'foo',
            "body" => 'bar',
            "userId" => 1
        ], 'posts/1')->get();

        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('title', $item);
        $this->assertArrayHasKey('body', $item);
        $this->assertArrayHasKey('userId', $item);

        $this->assertEquals('bar', $item['body']);
    }

    public function testCanDoPatchRequest()
    {
        $item = $this->client->patch([
            "body" => 'bar',
        ], 'posts/1')->get();

        $this->assertArrayHasKey('id', $item);
        $this->assertArrayHasKey('title', $item);
        $this->assertArrayHasKey('body', $item);
        $this->assertArrayHasKey('userId', $item);

        $this->assertEquals('bar', $item['body']);
    }

    public function testCanDoDeleteRequest()
    {
        $item = $this->client->delete(null, 'posts/1')->get();
        $this->assertEmpty($item);
    }

    public function testCanProvideCustomResponseClass()
    {
        /**
         * @var $post Cr
         */
        $post = $this->client->setResponseClass(Cr::class)->get('posts/1');

        $this->assertInstanceOf(JsonClient\ResponseInterface::class, $post);
        $this->assertInstanceOf(Cr::class, $post);

        $this->assertNotEmpty($post->result);
        $this->assertEquals(1, $post->result['id']);
    }

    /**
     * @expectedException \Error
     * @expectedExceptionMessage Response class have to implement interface of JsonClient\ResponseInterface
     */
    public function testButCustomResponseClassHasToFulfillProperInterface()
    {
        $this->client->setResponseClass(\stdClass::class)->get('posts/1');
    }

    protected function setUp()
    {
        parent::setUp();

        $this->client = new JsonClient('https://jsonplaceholder.typicode.com', true,
            [
                CURLOPT_HTTPHEADER => [
                    'X-Custom-Header: API-TOKEN'
                ]
            ]
        );
    }
}

class Cr extends JsonClient\Response
{
    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get();
    }
}
