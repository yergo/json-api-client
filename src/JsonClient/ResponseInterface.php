<?php
declare(strict_types=1);

namespace Yergo\JsonClient;

use Yergo\Curl;

/**
 * Interface ResponseInterface
 * @package Yergo\JsonClient
 */
interface ResponseInterface
{
    /**
     * ResponseInterface constructor.
     * @param Curl\RequestInterface $request
     */
    public function __construct(Curl\RequestInterface $request);

    /**
     * Gets the content of a request.
     * @return mixed
     */
    public function get();

    /**
     * Checks if response from server is already available.
     * @return bool
     */
    public function isAvailable(): bool;
}