<?php
declare(strict_types=1);

namespace Yergo\JsonClient;

use Yergo\Curl;

/**
 * Class Response
 * @package Yergo\JsonClient
 */
class Response implements ResponseInterface
{
    /**
     * @var Curl\RequestInterface
     */
    protected $request;

    /**
     * Response constructor.
     * @param Curl\RequestInterface $request
     */
    public function __construct(Curl\RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Gets the content of a request.
     * @return mixed
     */
    public function get()
    {
        return $this->request->response()->content();
    }

    /**
     * Checks if response is already available from server.
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->request->isFinished();
    }
}
