<?php

namespace Yergo;

use Yergo\JsonClient\Response;
use Yergo\JsonClient\ResponseInterface;

class JsonClient
{

    /**
     * @var string Url to the resource - the constant part, w/o ending slash.
     */
    protected $url;

    /**
     * @var Curl
     */
    protected $server;

    /**
     * @var string Class to be instantiated to provide result
     */
    protected $responseClass = Response::class;

    /**
     * JsonClient constructor.
     *
     * @param string $url
     * @param bool $asArrays
     * @param array $configuration compatible with {@see curl_setopt_array}
     */
    public function __construct(string $url, bool $asArrays = false, array $configuration = [])
    {
        $translator = new Curl\Translators\Json();
        if ($asArrays) {
            $translator->setResultsToArrays();
        } else {
            $translator->setResultsToObjects();
        }

        $configuration[CURLOPT_RETURNTRANSFER] = true;

        $this->url = $url;
        $this->server = (new Curl())
            ->setContentTranslator($translator)
            ->configure($configuration);
    }

    /**
     * Set up a class to be delivered as a result. Has to be an instance of Yergo\JsonClient\ResponseInterface
     *
     * @param string $name the name of class to be delivered as a result carrier.
     * @return JsonClient
     * @throws \Error
     */
    public function setResponseClass(string $name = Response::class): self
    {
        $reflection = new \ReflectionClass($name);
        if (!$reflection->implementsInterface(ResponseInterface::class)) {
            throw new \Error('Response class have to implement interface of JsonClient\ResponseInterface', E_ERROR);
        }

        $this->responseClass = $name;
        return $this;
    }

    private function responseFactory(Curl\RequestInterface $request): ResponseInterface
    {
        return new $this->responseClass($request);
    }

    /**
     * @param mixed $question array or JSON-able object
     * @param string $path
     * @param string $method
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function ask($question, string $path = NULL, string $method = 'POST', array $configuration = [])
    {
        $configuration[CURLOPT_CUSTOMREQUEST] = $method;
        $configuration[CURLOPT_URL] = $this->url . ($path ? ('/' . $path) : '');
        $configuration[CURLOPT_HTTPHEADER][] = 'Content-Type: application/json';

        $request = $this->server->send($question, $configuration);

        return $this->responseFactory($request);
    }

    /**
     * @param string $path
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function get(string $path = NULL, array $configuration = [])
    {
        return $this->ask(null, $path, 'GET', $configuration);
    }

    /**
     * @param mixed $question array or JSON-able object
     * @param string $path
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function post($question, string $path = NULL, array $configuration = [])
    {
        return $this->ask($question, $path, 'POST', $configuration);
    }

    /**
     * @param mixed $question array or JSON-able object
     * @param string $path
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function put($question, string $path = NULL, array $configuration = [])
    {
        return $this->ask($question, $path, 'PUT', $configuration);
    }

    /**
     * @param mixed $question array or JSON-able object
     * @param string $path
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function patch($question, string $path = NULL, array $configuration = [])
    {
        return $this->ask($question, $path, 'PATCH', $configuration);
    }

    /**
     * @param mixed $question array or JSON-able object
     * @param string $path
     * @param array $configuration compatible with {@see curl_setopt_array}
     * @return ResponseInterface
     */
    public function delete($question, string $path = NULL, array $configuration = [])
    {
        return $this->ask($question, $path, 'DELETE', $configuration);
    }
}