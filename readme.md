# yergo/json-api-client

## Idea

This class wraps `yergo/curl` to make parallel multiple curl requests as easy as possible.

## Installation

To install and use library you should include it in your composer.json file. You can achieve it in two ways, by adding
it by hand within `require` section:


```json
{
    "require": {
        "yergo/json-api-client" : "*"
    }
}
```    
    
or with a command line:

```bash
composer require json-api-client
```

## Usage

Configure your API endpoint:

```php
use Yergo\JsonClient;

$api = new JsonClient(
    // so called URL, but really a part of an url that is not really changing between requests
    'https://api.host.com/public',
    // should you unparse json to arrays
    false,
    // CURLOPT_* if necessary.
    [
        CURLOPT_HTTPHEADER => [
            'X-Custom-Header: API-TOKEN'
        ]
    ]
);
```

And use it in a way you want:

```php
$listResponse = $api->get('products/list');

// ....

$listResponse->get(); // blocks (waits) for request to receive content
```

```php
$items = [
    // ...
];

$responses = [];

foreach($items as $item) {
    $responses[] = $api->post($item, 'products');
}

// above requests will proceed during the code execution beneath,
// but if you need to ensure their done:

foreach ($responses as $response) {
    $result = $response->get();
    // ...
}
```

Note, that it's `::get()` method that blocks until request finish, so it will perform better to first send all
requests, and take results afterwards, than send and receive at same place.
